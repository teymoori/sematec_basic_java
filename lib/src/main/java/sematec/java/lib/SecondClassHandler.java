package sematec.java.lib;

import static sematec.java.lib.MySecondClass.getAge;

public class SecondClassHandler {
    public static void main(String[] n) {
        MySecondClass secondClass
                = new MySecondClass();
        String name = secondClass.getStudentName();

        int age = getAge();
        SecondClassHandler.log("tehran");
    }
    public static void log(String msg) {
        System.out.print(msg);
    }

}
