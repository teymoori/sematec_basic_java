package sematec.java.lib;

public class MySecondClass {

    public String getStudentName() {
        return "AmirHossein " + isIranian();
    }

    public boolean isIranian() {
        return false;
    }

    public static int getAge() {
        return 40;
    }


}
