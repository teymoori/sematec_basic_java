package sematec.java.lib.arrays;

import sematec.java.lib.utils.PublicMethods;

public class MainArrays {


    public static void main(String[] ar) {
        String[] names = getNames();
        PublicMethods.printResult(names[1]);

        String[] friends = {"ali", "reza", "negin"};
        PublicMethods.printResult(
                returnSecondValue(
                        friends
                )
        );

        String value = "20" ;
        int ageValue =
                Integer.parseInt(value) ;
        String isIranian = "true" ;
        boolean is = Boolean.parseBoolean(isIranian) ;

        String pNUmber = "3.14" ;
        float fp = Float.parseFloat(pNUmber) ;

        int age = 30 ;
        String myAge = String.valueOf(age) ;
        String mAge = age+"" ;


    }

    public static String[] getNames() {
        String[] names = {
                "Ali",
                "JAvad",
                "MAryam"
        };
        return names;
    }

    public static String returnSecondValue(String[] names) {
        return names[names.length-1];
    }

}
